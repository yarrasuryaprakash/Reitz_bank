// Update with your config settings.

// Update with your config settings.

module.exports = {
  development: {
    client: 'pg',
    connection: {
      database: 'reitz_data_bank',
      host: 'localhost',
      user: 'postgres',
      password: 'Design_20'
    }
  },

  staging: {
    client: 'pg',
    connection: {
      database: 'reitz_data_bank',
      user: 'postgres',
      password: 'Design_20'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'FanList'
    }
  },

  production: {
    client: 'pg',
    connection: {
      database: 'reitz_data_bank',
      user: 'postgres',
      password: 'Design_20'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'FanList'
    }
  }
};
