exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('FanList', function(table) {
      table.increments();
      table.string('rfNo');
      table.string('customerName');
      table.string('projectName');
      table.string('application');
      table.string('fanModel');
      table.double('flow');
      table.double('staticPressure');
      table.double('temperature');
      table.double('density');
      table.double('shaftPower');
      table.double('fanSpeed');
      table.double('motorPower');
      table.double('motorSpeed');
      table.double('yearOfMfg');
      table.string('price');
      table.boolean('isGaDrawing');
      table.boolean('isFanCurves');
      table.boolean('isActive');
      table.timestamps();
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([knex.schema.dropTable('FanList')]);
};
