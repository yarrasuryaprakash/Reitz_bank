var express = require('express');
const router = express.Router();
const knex = require('knex');
const dbConfig = require('../knexfile').development;
const xlsxtojson = require('xlsx-to-json-lc');
const xlstojson = require('xls-to-json-lc');
const multer = require('multer');
const fs = require('fs');
const path=require('path');
var t = require('tcomb-validation');
var validations =  require('./validations');
var PdfReader = require('pdfreader').PdfReader;
var PDFParser=require('pdf2json');
var pdfUtil = require('pdf-to-text');
var imageToTextDecoder = require('image-to-text');
var pdf_extract = require('pdf-extract');
var inspect = require('eyes').inspector({maxLength:20000});
const storage = multer.diskStorage({
  //multers disk storage settings
  destination: function(req, file, cb) {
    console.log('storage');
    cb(null, './uploads/');
  },
  filename: function(req, file, cb) {
    console.log('storage');

    cb(
      null,
      file.fieldname +
        '.' +
        file.originalname.split('.')[file.originalname.split('.').length - 1]
    );
  }
});


const upload = multer({
  //multer settings
  storage: storage,
  limits: {
    fileSize: 52428800
  },
  fileFilter: function(req, file, callback) {
    //file filter
    console.log('abcdefgh');
    if (
      ['xls', 'xlsx'].indexOf(
        file.originalname.split('.')[file.originalname.split('.').length - 1]
      ) === -1
    ) {
      return callback(new Error('Wrong extension type'));
    }
    callback(null, true);
  }
}).single('fanResults');

router.post('/', function(req, res) {
  req.setTimeout(0);
  var exceltojson;
  upload(req, res, function(err) {
    if (err) {
      res.json({ error_code: 1, err_desc: err });
      return;
    }
 
    if (!req.file) {
      res.json({ error_code: 1, err_desc: 'No file passed' });
      return;
    }
   
 
    if (
      req.file.originalname.split('.')[
        req.file.originalname.split('.').length - 1
      ] === 'xlsx'
    ) {    
      exceltojson = xlsxtojson;
    } else {
      exceltojson = xlstojson;
    }
    
    
    try {
      exceltojson(
        {
          input: req.file.path, //the same path where we uploaded our file
          output: null, //since we don't need output.json
          lowerCaseHeaders: false
        },
        function(err, result) {
          if (err) {
            return res.json({ error_code: 1, err_desc: err, data: null });
          }
         
          const result1 = result
            .filter(o => o['RF No.'] != null && o['RF No.'] != '')
            .map(o => ({
              rfNo: o['RF No.'],
              customerName: o['Customer Name'],
              projectName: o['Project Name'],
              application: o['Application'],
              fanModel: o['Fan Model'],
              flow: isNaN(parseFloat(o['Flow       m3 / hr']))
                ? 0
                : o['Flow       m3 / hr'],
              staticPressure: isNaN(parseFloat(o['Static Pressure mm WG']))
                ? 0
                : o['Static Pressure mm WG'],
              temperature: isNaN(parseFloat(o['Temp 0C'])) ? 0 : o['Temp 0C'],
              density: isNaN(parseFloat(o['Density  Kg/ m3']))
                ? 0
                : o['Density  Kg/ m3'],
              shaftPower: isNaN(parseFloat(o['Shaft     Power        Kw']))
                ? 0
                : o['Shaft     Power        Kw'],
              fanSpeed: isNaN(parseFloat(o['Fan     Speed  RPM']))
                ? 0
                : o['Fan     Speed  RPM'],
              motorPower: isNaN(parseFloat(o['Motor Power    Kw']))
                ? 0
                : o['Motor Power    Kw'],
              motorSpeed: isNaN(parseFloat(o['Motor     Speed       RPM']))
                ? 0
                : o['Motor     Speed       RPM'],
              yearOfMfg: isNaN(
                parseFloat(o['Year                of                Mfg.'])
              )
                ? 0
                : o['Year                of                Mfg.'],
              price: o['Price / Fan'],
              isGaDrawing: o['GA Drawing'] === 'Yes' ? true : false,
              isFanCurves: o['Fan Curves'] === 'Yes' ? true : false,
              isActive: true,
              created_at: new Date().toISOString(),
              updated_at: new Date().toISOString()
            }));
         
            
          knex(dbConfig)
            .transaction(function(tr) {
              return knex(dbConfig)
                .batchInsert('FanList', result1)
                .transacting(tr);
            })
            .then(() => {
              return res.status(200).json({ message: 'success' });
            })
            .catch(error => {
              return res.status(500).json({ message: error.message });
            });
        }
      );
    } catch (e) {
      res.json({ error_code: 1, err_desc: 'Corrupted excel file' });
    }
  });
 });



const getRangeCurve = fileName => {
     
    
  if (fileName === 'RF-16407 to RF-16409_Performance curve(SPD).pdf') {
    const values = fileName.split('to');
    const start = values[0].split(/[-,_]/)[1].replace(/\s+/g, '');
    const end = values[1].split(/[-,_]/)[1].replace(/\s+/g, '');
    return { file: fileName, start: start, end: end };
  }
  if (fileName.indexOf('&') === -1 && fileName.indexOf('to') !== -1) {
    const values = fileName.split('to');
    const start = values[0].split(/[-,_]/)[1].replace(/\s+/g, '');
    const end =
      start.substring(0, 2) + values[1].split(/[-,_]/)[0].replace(/\s+/g, '');
    return { file: fileName, start: start, end: end };
  }
  
  if (fileName.indexOf('&') !== -1 && fileName.indexOf('to') === -1) {
    const values = fileName.split('&');
    const start = values[0].split(/[-,_]/)[1].replace(/\s+/g, '');
    const end = values[1].split(/[-,_]/)[1].replace(/\s+/g, '');
    return { file: fileName, start: start, end: end };
  }
  if (fileName.indexOf('&') === -1 && fileName.indexOf('to') === -1) {
    const value = fileName.split(/[-,_]/)[1];
    
    return { file: fileName, start: value, end: value };
  }
  if (fileName.indexOf('&') !== -1 && fileName.indexOf('to') !== -1) {
    const values = fileName.split('to');
    const start = values[0].split(/[-,_]/)[1].replace(/\s+/g, '');
    const end =
      start.substring(0, 2) + values[1].split(/[-,_]/)[0].replace(/\s+/g, '');
    return { file: fileName, start: start, end: end };
  }
};


router.get('/curves', (req, res) => {
  let curvFiles = [];
  fs.readdir('./Curves/', (err, files) => {
    if (!err) {
      curvFiles = files;
    
      const rangeCurvedFiles = curvFiles.filter(
        x =>
          parseInt(req.query.rfNo) >= getRangeCurve(x).start &&
          parseInt(req.query.rfNo) <= getRangeCurve(x).end
      );
      if(rangeCurvedFiles.length===0) return res.status(404).json({
        message: "file not found"
      })

      return res.json(rangeCurvedFiles);
    }
  });
});

const getRangeGa = fileName => {
  if (fileName.indexOf('&') === -1 && fileName.indexOf('to') !== -1) {
    const values = fileName.split('to');
    const start = values[0].replace(/\s+/g, '');
    const end = values[1].split(/[-,_]/)[0].replace(/\s+/g, '');
    return { file: fileName, start: start, end: end };
  }
  if (fileName.indexOf('&') !== -1 && fileName.indexOf('to') === -1) {
    const values = fileName.split('&');
    const start = values[0].replace(/\s+/g, '');
    const end = values[1].split(/[-,_]/)[0].replace(/\s+/g, '');
    return { file: fileName, start: start, end: end };
  }
  if (fileName.indexOf('&') === -1 && fileName.indexOf('to') === -1) {
    const value = fileName.split(/[-,_]/)[0];
    
    return { file: fileName, start: value, end: value };
  }
};

router.get('/gaDrawnings', (req, res) => {
  let gaDrawningFiles = [];
  fs.readdir('./GA Drawnings/', (err, files) => {
    if (!err) {
      const values = [];
      gaDrawningFiles = files;
      const rangeGaDrawningFiles = gaDrawningFiles.filter(
        x =>
          parseInt(req.query.rfNo) >= getRangeGa(x).start &&
          parseInt(req.query.rfNo) <= getRangeGa(x).end
      );
      if(rangeGaDrawningFiles.length===0) return res.status(404).json({
        message: "file not found"
      })
      
      rangeGaDrawningFiles.forEach(function(filename) {
        console.log(filename)
        fs.readFile('./GA Drawnings/' + filename, 'utf-8', function(err, content) {
        
          if (err) {
            return err;
          }
         return res.status(200).send(content)
        });

      })
    }
  });
});

const getRangePO = fileName => {
  if (
    fileName ===
    'Deepak Cements_080816_RG_RF 9296, 9297 & 9503 Amended PO0001.pdf'
  ) {
    const values = fileName.split('&');
    const start = values[0].split(/[,,_]/)[3].split(' ')[1];
    const end = values[1].split(' ')[1];
    return { file: fileName, start: start, end: end };
  }
  if (fileName === 'EEL India_080819_RG_RF 9553 & 54 PO0001.pdf') {
    const values = fileName.split('&');
    const start = values[0].split('_')[3].split(' ')[1];
    const end = start.substring(0, 2) + values[1].split(' ')[1];
    return { file: fileName, start: start, end: end };
  }
  if (fileName.indexOf('&') === -1 && fileName.indexOf('to') !== -1) {
    const values = fileName.split('to');
    const start = values[0].split('_')[3].split(' ')[1];
    const end =
      start.substring(0, 2) + values[1].split(' ')[1].replace(/\s+/g, '');
    return { file: fileName, start: start, end: end };
  }
  if (fileName.indexOf('&') !== -1 && fileName.indexOf('to') === -1) {
    const values = fileName.split('&');
    const start = values[0].split('_')[3].split(' ')[1];
    const end = values[1].split(' ')[1];
    return { file: fileName, start: start, end: end };
  }
  if (fileName.indexOf('&') === -1 && fileName.indexOf('to') === -1) {
    const value = fileName.split('_')[3].split(' ')[1];
   
    return { file: fileName, start: value, end: value };
  }
 };
router.get('/po', (req, res) => {
  let poFiles = [];
  fs.readdir('./PO/', (err, files) => {
    if (!err) {
      poFiles = files;
      const rangePoFiles = poFiles.filter(
        x =>
          parseInt(req.query.rfNo) >= getRangePO(x).start &&
          parseInt(req.query.rfNo) <= getRangePO(x).end
      );
      if(rangePoFiles.length===0) return res.status(404).json({
        message: "file not found"
      })
      return res.json(rangePoFiles);
    }

  });
});


// TO GET ALL 3 TYPES OF FILES RELATED TO RF NUMBER
router.get('/files', (req, res) => {
  let poFiles = [];
  fs.readdir('./PO/', (err, files) => {
    if (!err) {
      poFiles = files;
      const rangePoFiles = poFiles.filter(
        x =>
          parseInt(req.query.rfNo) >= getRangePO(x).start &&
          parseInt(req.query.rfNo) <= getRangePO(x).end
      );
  let gaDrawningFiles = [];
      fs.readdir('./GA Drawnings/', (err, files) => {
        if (!err) {
          const values = [];
          gaDrawningFiles = files;
          const rangeGaDrawningFiles = gaDrawningFiles.filter(
            x =>
              parseInt(req.query.rfNo) >= getRangeGa(x).start &&
              parseInt(req.query.rfNo) <= getRangeGa(x).end
          );
          let curvFiles = [];
          fs.readdir('./Curves/', (err, files) => {
            if (!err) {
              curvFiles = files;
              const rangeCurvedFiles = curvFiles.filter(
                x =>
                  parseInt(req.query.rfNo) >= getRangeCurve(x).start &&
                  parseInt(req.query.rfNo) <= getRangeCurve(x).end
              );
              
              return res.json({
                curves: rangeCurvedFiles,
                GaDrawning: rangeGaDrawningFiles,
                PO: rangePoFiles
              });
            }
          });
        }
      });
    }
  });
});

const reqParamsValidation=(req, res, next) => {
    let result = t.validate(req.params,validations.downloadValid);
      if (result.isValid()) next();
    else res.status(400).json(result.errors);
   };

// To download files of Curves , GA drawning, PO files
router.get('/:type',reqParamsValidation, (req, res) => {          
    if (req.params.type === 'poDownload') {
      let poFiles = [];
      fs.readdir('./PO/', (err, files) => {
        if (!err) {
          poFiles = files;
          const rangePoFiles = poFiles.filter(
            x =>
              parseInt(req.query.rfNo) >= getRangePO(x).start &&
              parseInt(req.query.rfNo) <= getRangePO(x).end
          );
          const fileName = rangePoFiles[0];
          if(!fileName) return res.status(404).json({
            message: "file not found"
          })
          var filepath = path.join('./PO', fileName);
   
          res.download(filepath, fileName);
        }
      });
    } else if (req.params.type === 'gadrawningDownload') {
      let gaDrawningFiles = [];
      fs.readdir('./GA Drawnings/', (err, files) => {
        if (!err) {
          gaDrawningFiles = files;
          const rangeGaDrawningFiles = gaDrawningFiles.filter(
            x =>
              parseInt(req.query.rfNo) >= getRangeGa(x).start &&
              parseInt(req.query.rfNo) <= getRangeGa(x).end
          );
          const fileName = rangeGaDrawningFiles[0];
          if(!fileName) return res.status(404).json({
            message: "file not found"
          })
          var filepath = path.join('./GA Drawnings', fileName);
   
          res.download(filepath, fileName);
        }
      });
    } else if (req.params.type === 'curvesDownload') {
      let curvFiles = [];
      
      fs.readdir('./Curves/', (err, files) => {
        if (!err) {
          curvFiles = files;
          const rangeCurvedFiles = curvFiles.filter(
            x =>
              parseInt(req.query.rfNo) >= getRangeCurve(x).start &&
              parseInt(req.query.rfNo) <= getRangeCurve(x).end
          );
         
          const fileName = rangeCurvedFiles[0];
          if(!fileName) return res.status(404).json({
            message: "file not found"
          })
          var filepath = path.join('./Curves', fileName);
   
          res.download(filepath, fileName);
        }
      });
    }
   });
// testing
  // const reqBodyValidation=(req, res, next) => {
  //   let result = t.validate(req.body,validations.data);
  //     if (result.isValid()) next();
  //   else res.status(400).json(result.errors);
  //  };

// //Data Insertion 
//     router.post('/data',reqBodyValidation, function (req, res) {
//       knex(dbConfig)
//     .returning('*')
//     .insert(req.body)
//     .into('FanList')count
//     .then((data) =>{}
//       return res.status(200).json(data);
//     })
//     .catch((e) =>{
//       return res.status(500).json(e)
//     })
//  });


const reqQueryValidation=(req, res, next) => {
  let result = t.validate(req.query,validations.pagination);
   if (result.isValid()) next();
  else res.status(400).json(result.errors);
 };

// PAGINATION
router.get('/',reqQueryValidation, (req, res) => {
  // console.log(typeof req.query.pageSize)
    const db = knex(dbConfig);
    const pageSize = parseInt(req.query.pageSize)
    let index = parseInt(req.query.index)

    // console.log(typeof index)
     const query =  db('FanList')
       .select('*',db.raw(`split_part(\"FanList\".\"fanModel\",' ',1) as "fanType"`))
       const Count =  db('FanList')
       .count('id')
     if (req.query.rfNo && req.query.rfNo!== '')
       {
         query.where('rfNo','ilike','%'+req.query.rfNo+'%')
         Count.where('rfNo','ilike','%' + req.query.rfNo + '%')
      }
     if (req.query.fanModel && req.query.fanModel !== '')
       {
         query.where('fanModel','ilike','%'+req.query.fanModel+'%')
         Count.where('fanModel','ilike','%' + req.query.fanModel + '%')
      }
      
      if (req.query.fanType && req.query.fanType !== '')
       {
         query.where(db.raw("split_part(\"FanList\".\"fanModel\",' ',1) ilike ?",['%'+req.query.fanType+'%']))
         Count.where(db.raw("split_part(\"FanList\".\"fanModel\",' ',1) ilike ?",['%'+req.query.fanType+'%']))
      }

      if (req.query.customerName && req.query.customerName !== '')
       {
         query.where('customerName','ilike','%'+req.query.customerName+'%')
         Count.where('customerName','ilike','%' + req.query.customerName + '%')
      }

      if (req.query.staticPressure && req.query.staticPressure !== '')
       {
         query.where('staticPressure',req.query.staticPressure)
         Count.where('staticPressure',req.query.staticPressure)
      }

      if (req.query.price && req.query.price !== '')
      {
        query.where('price',req.query.price)
        Count.where('price',req.query.price)
     }

     if (req.query.temperature && req.query.temperature !== '')
       {
         query.where('temperature',req.query.temperature)
         Count.where('temperature',req.query.temperature)
      }

      if (req.query.shaftPower && req.query.shaftPower !== '')
      {
        query.where('shaftPower',req.query.shaftPower)
        Count.where('shaftPower',req.query.shaftPower)
     }

     if (req.query.fanSpeed && req.query.fanSpeed !== '')
      {
        console.log('Hello');
        query.where('fanSpeed',req.query.fanSpeed)
        Count.where('fanSpeed',req.query.fanSpeed)
     }
    const promData = Promise.all([query.limit(pageSize).offset(index * pageSize),
         Count])
      .then(results => {
       db.destroy();
       return res.json({ 
        data: results[0],
        count: parseInt(results[1][0].count)  
      });
    }).catch( e => {
      console.log(query.toSQL())
      console.log(e);
      return res.status(500).json(e)
    })
  })  

  router.get('/gadrawning/sortbyname', (req, res) => {
    let gaDrawningFiles = [];
    fs.readdir('./GA Drawnings/', (err, files) => {
      if (!err) {
        const values = [];
        gaDrawningFiles = files.sort();
        // const rangeGaDrawningFiles = gaDrawningFiles.filter(
        //   x =>
        //     parseInt(req.query.rfNo) >= getRangeGa(x).start &&
        //     parseInt(req.query.rfNo) <= getRangeGa(x).end
        // );
        if(gaDrawningFiles.length===0) return res.status(404).json({
          message: "file not found"
        })
        return res.json(gaDrawningFiles);
      }
    });
  });



  const getRangeCurve2 = fileName => {

    if (!fileName.includes('&')) {
     const filesall=[];
     let start=fileName.split('_RG_RF ')[1].split(' ')[0]
     let end=fileName.split('_RG_RF ')[1].split(' ')[0]
     return { file: fileName, start: start, end: end };
    }
   
   };
  router.get('/curves/sortbyname', (req, res) => {
    let gaDrawningFiles = [];
    fs.readdir('./Curves/', (err, files) => {
      if (!err) {
        const values = [];
        curvesFiles = files.sort().filter(x=>x.includes('&') && !x.includes('TO'));
  //      curvesFiles = files.sort().filter(x=>x.includes('&'));
  //      curvesFiles = files.sort().filter(x=>x.includes('to') && !x.includes('&'));
  //      const rangeGaDrawningFiles = curvesFiles.filter(x=>getRangeCurve2(x)!==undefined)
      
  // const filterData=rangeGaDrawningFiles.filter(x=>parseInt(req.query.rfNo) >=getRangeCurve2(x).start &&
  //       parseInt(req.query.rfNo) <=getRangeCurve2(x).end)
  //       if(filterData.length===0) return res.status(404).json({
  //         message: "file not found"
  //       })
        return res.json(curvesFiles);
      }
    });
  });

  
  module.exports = router; 
